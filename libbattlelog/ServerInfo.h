#ifndef SERVERINFO_H
#define SERVERINFO_H

#include "libbattlelog.h"


class ServerInfo : public LBL::IServerInfo
{
public:
    std::uint64_t getGameId() const override
    {
        return m_gameId;
    }
    const char *getIP() const override
    {
        return m_ip.c_str();
    }
    std::uint16_t getPort() const override
    {
        return m_port;
    }
    std::vector<Team> getTeams() const override
    {
        return m_teams;
    }

    std::string m_country;
    bool m_hasFairfight = false;
    // expansions
    std::uint64_t m_gameId = 0;
    std::string m_guid;
    bool m_hasPassword = false;
    std::string m_ip;
    std::uint16_t m_port = 0;
    std::string m_mapName;
    int m_mapMode = 0;
    std::string m_name;
    int m_ping = 0;
    bool m_hasPunkbuster = false;
    bool m_ranked = false;
    // region
    // slots

    std::string m_modeName;

    std::vector<Team> m_teams;

};

#endif