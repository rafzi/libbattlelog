#ifndef GAMESERVERCOMM_H
#define GAMESERVERCOMM_H

#include "ServerInfo.h"


// requests data about a specific match directly from the gameserver
void GetChallengeData(ServerInfo *server);

#endif