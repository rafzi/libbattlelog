#include "libbattlelog.h"
#include "BLRequest.h"
#include "ServerInfo.h"
#include "GameServerComm.h"
#include "Filter.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTMLForm.h"
#include "Poco/InflatingStream.h"
#include "Poco/StreamCopier.h"
#include "Poco/JSON/Parser.h"
#include "Poco/Util/WinRegistryKey.h"
#include "Poco/Process.h"
#include <sstream>
#include <thread>

#include <shellapi.h>


class LBL::BLSession::BLSessionImpl
{
    friend LBL::BLSession;
public:
    BLSessionImpl() : m_session("battlelog.battlefield.com", Poco::Net::HTTPSClientSession::HTTPS_PORT, new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", Poco::Net::Context::VERIFY_NONE, 9, true))
    {
        m_session.setKeepAlive(true);
        //m_session.setProxy("localhost", 8888); // fiddler debugging!
    }

    std::unique_ptr<Poco::Net::HTTPResponse> submitForm(Poco::Net::HTTPRequest& request, Poco::Net::HTMLForm& form, std::string& content)
    {
        auto response = std::make_unique<Poco::Net::HTTPResponse>();

        try {
            form.prepareSubmit(request);
            std::ostream& ostr = m_session.sendRequest(request);
            form.write(ostr);
            std::istream& istr = m_session.receiveResponse(*response.get());
            if (response->get("Content-Encoding", "") == "gzip")
            {
                Poco::InflatingInputStream gzipStr(istr, Poco::InflatingStreamBuf::STREAM_GZIP);
                Poco::StreamCopier::copyToString(gzipStr, content);
            } else {
                Poco::StreamCopier::copyToString(istr, content);
            }

        } catch (Poco::Exception& e) {
            printf("%s - %s\n", e.className(), e.displayText().c_str());
        }

        return response;
    }

    bool addLoginCookie(Poco::Net::HTTPRequest& request)
    {
        if (m_loginCookie.getValue() == "")
            return false;

        Poco::Net::NameValueCollection cookies;
        cookies.add(m_loginCookie.getName(), m_loginCookie.getValue());
        request.setCookies(cookies);
        return true;
    }

private:
    Poco::Net::HTTPSClientSession m_session;
    Poco::Net::HTTPCookie m_loginCookie;

};


LBL::BLSession::BLSession() : m_impl(new BLSessionImpl)
{
}

LBL::BLSession::~BLSession()
{
    delete m_impl;
}


std::unique_ptr<LBL::IFilter> LBL::CreateFilter()
{
    return std::make_unique<Filter>();
}


std::vector<std::unique_ptr<LBL::IServerInfo>> LBL::BLSession::getServers(const IFilter *filter)
{
    BLRequest request("GET", "/bf4/servers/pc/");
    request.add("X-Requested-With", "XMLHttpRequest");
    request.add("X-AjaxNavigation", "1");

    auto form = dynamic_cast<const Filter*>(filter)->generateForm();

    std::string content;
    m_impl->submitForm(request, *form.get(), content);

    std::vector<std::unique_ptr<LBL::IServerInfo>> servers;

    try {
        Poco::JSON::Parser parser;
        auto json = parser.parse(content);

        const Poco::DynamicStruct& root = *json.extract<Poco::JSON::Object::Ptr>();

        std::vector<std::thread> tasks;
        for (const auto& server : root["globalContext"]["servers"].extract<std::vector<Poco::Dynamic::Var>>())
        {
            auto entry = std::make_unique<ServerInfo>();
            entry->m_country = server["country"].toString();
            entry->m_hasFairfight = server["fairfight"];
            entry->m_gameId = server["gameId"];
            entry->m_guid = server["guid"].toString();
            entry->m_hasPassword = server["hasPassword"];
            entry->m_ip = server["ip"].toString();
            entry->m_port = server["port"];
            entry->m_mapName = server["map"].toString();
            entry->m_mapMode = server["mapMode"];
            entry->m_name = server["name"].toString();
            entry->m_ping = server["ping"];
            entry->m_hasPunkbuster = server["punkbuster"];
            entry->m_ranked = server["ranked"];

            auto pServer = entry.get();
            tasks.push_back(std::thread([pServer](){
                GetChallengeData(pServer);
            }));

            servers.push_back(std::move(entry));
        }

        for (auto& task : tasks)
        {
            task.join();
        }

    } catch (Poco::Exception& e) {
        printf("%s - %s\n", e.className(), e.displayText().c_str());
    }

    return servers;
}


bool LBL::BLSession::login(const std::string& email, const std::string& password)
{
    BLRequest request("POST", "/bf4/gate/login/");

    Poco::Net::HTMLForm form;
    form.add("redirect", "|bf4|");
    form.add("email", email);
    form.add("password", password);
    form.add("remember", "1");
    form.add("submit", "Log in");

    std::string content;
    auto response = m_impl->submitForm(request, form, content);

    std::vector<Poco::Net::HTTPCookie> cookies;
    response->getCookies(cookies);
    for (const auto& cookie : cookies)
    {
        if (cookie.getName() == "beaker.session.id")
        {
            m_impl->m_loginCookie = cookie;
        }
    }

    return true;
}


std::string LBL::BLSession::getSoldierId()
{
    BLRequest request("GET", "/bf4/launcher/playablepersona/");

    if (!m_impl->addLoginCookie(request))
    {
        printf("login required!\n");
        return "";
    }

    std::string content;
    auto response = m_impl->submitForm(request, Poco::Net::HTMLForm(), content);

    try {
        Poco::JSON::Parser parser;
        auto json = parser.parse(content);

        const Poco::DynamicStruct& root = *json.extract<Poco::JSON::Object::Ptr>();
        return root["data"]["personaId"];

    } catch (Poco::Exception& e) {
        printf("%s - %s\n", e.className(), e.displayText().c_str());
    }

    return "";
}


std::string LBL::BLSession::getAuthCode(const std::string& soldierId)
{
    std::string url = "/bf4/launcher/token/1/";
    url += soldierId;
    BLRequest request("GET", url);

    if (!m_impl->addLoginCookie(request))
    {
        printf("login required!\n");
        return "";
    }

    std::string callback = "jQuery173005857757285050232_1396687899025";
    Poco::Net::HTMLForm form;
    form.add("callback", callback);
    form.add("_", "1396687852281");

    std::string content;
    auto response = m_impl->submitForm(request, form, content);
    if (!response || response->getStatus() != Poco::Net::HTTPResponse::HTTP_OK)
    {
        printf("login failed!\n");
        return "";
    }

    // extract json string from js code
    content = content.substr(0, content.find_last_of('}')+1);
    content = content.substr(content.find_first_of('{'));

    try {
        Poco::JSON::Parser parser;
        auto json = parser.parse(content);

        const Poco::DynamicStruct& root = *json.extract<Poco::JSON::Object::Ptr>();
        return root["data"]["authCode"];

    } catch (Poco::Exception& e) {
        printf("%s - %s\n", e.className(), e.displayText().c_str());
    }

    return "";
}


void LBL::BLSession::reserveSlot(const IServerInfo *server, const std::string& soldierId)
{
    std::string url = "/bf4/launcher/reserveslotbygameid/1/";
    url += soldierId;
    url += "/";
    url += std::to_string(server->getGameId());
    url += "/1/";
    url += "24355a5d-2da4-4587-92e8-9649a85ab1cc"; // randomly choosen
    BLRequest request("POST", url);

    if (!m_impl->addLoginCookie(request))
    {
        printf("login required!\n");
        return;
    }

    Poco::Net::HTMLForm form;
    form.add("post-check-sum", m_impl->m_loginCookie.getValue().substr(0, 10));

    std::string content;
    auto response = m_impl->submitForm(request, form, content);

    // TODO check and return if ok
}


void LBL::BLSession::launchGame(const IServerInfo *server, const std::string& authCode, const std::string& soldierId)
{
    Poco::Util::WinRegistryKey regkey("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\EA Games\\Battlefield 4", true);
    std::string installdir = regkey.getString("Install Dir");

    Poco::Util::WinRegistryKey regkeyplug("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Battlelog Web Plugins", true);
    std::string installdirplug = regkeyplug.getString("");

    std::stringstream params;
    params << "\"<data putinsquad=\\\"true\\\" gameid=\\\""
        << server->getGameId()
        << "\\\" role=\\\"soldier\\\" personaref=\\\""
        << soldierId
        << "\\\" levelmode=\\\"mp\\\"></data>\"";

    std::string args;
    args += "NO|SW_SHOWNA|";
    args += installdir;
    args += "bf4.exe|bf4.exe|";
    args += installdir;
    args += "|-webMode MP -Origin_NoAppFocus -AuthCode ";
    args += authCode;
    args += " -requestState State_ClaimReservation -requestStateParams ";
    args += params.str();

    ShellExecute(0, "open", "C:\\Program Files (x86)\\Battlelog Web Plugins\\esnlauncher4.exe",
        args.c_str(), 0, SW_SHOWNORMAL);
}