#include "GameServerComm.h"
#include "Poco/Net/DatagramSocket.h"
#include "Poco/ByteOrder.h"


void Deserialize(const unsigned char *buffer, int len, ServerInfo *server)
{
    int modeLen = buffer[0x1b];
    char modeName[128];
    memcpy(modeName, &buffer[0x1c], modeLen);
    modeName[modeLen] = '\0';

    server->m_modeName = modeName;

    int teamsBase = 0x1c + modeLen + 0x2;
    int numTeams = buffer[teamsBase - 1] & 0x7;

    server->m_teams.resize(numTeams);

    for (int i = 0; i < numTeams; i++)
    {
        int teamNr = buffer[teamsBase + i*5];
        int teamPoints = Poco::ByteOrder::fromBigEndian(*(uint16_t*)(buffer + teamsBase + i*5 + 1));
        int teamMaxPoints = Poco::ByteOrder::fromBigEndian(*(uint16_t*)(buffer + teamsBase + i*5 + 3));

        server->m_teams[i].number = teamNr;
        server->m_teams[i].points = teamPoints;
        server->m_teams[i].maxPoints = teamMaxPoints;
    }
}

void GetChallengeData(ServerInfo *server)
{
    Poco::Net::SocketAddress serverAddr(server->getIP(), server->getPort());

    try {
        Poco::Net::SocketAddress sender;
        Poco::Net::DatagramSocket socket;
        unsigned char response[4096];
        int recvd = 0;

        unsigned char request1[15] = "\xff\xff\xff\xff\x51\x50\x5f";
        unsigned char request2[19] = "\xff\xff\xff\xff\x51\x50\x5f";

        *(std::uint64_t*)(request1 + 7) = Poco::ByteOrder::flipBytes(server->getGameId());

        socket.sendTo(request1, 15, serverAddr);
        recvd = socket.receiveFrom(response, 1024, sender);

        for (int i = 0; i < 12; i++)
            request2[i+7] = response[i];

        socket.sendTo(request2, 19, serverAddr);
        recvd = socket.receiveFrom(response, 4096, sender);

        Deserialize(response, recvd, server);

    } catch (Poco::Exception& e) {
        printf("%s - %s\n", e.className(), e.displayText().c_str());
    }
}